<html>
<head>
	<title>Ejemplo operaciones de comparacion</title>
</head>
<body>
	<h1>Ejemplo operaciones de comparacion en PHP</h1>
	<?php 
		$a = 8;
		$b = 3;
		$c = 3;
		echo $a == $b, "<br>";
		echo $a != $b, "<br>";
		echo $a < $b, "<br>";
		echo $a > $b, "<br>";
		echo $a >= $c, "<br>";
		echo $a <= $c, "<br>";

		/*
			== Compara si las dos variables son iguales.
			!= Compara si las variavles no con del mismp tipo.
			<  Compara si A es menor que B.
			>  Compara si A es mayor que B.
			>= Compara si A es mayor o igual que B.
			<= Compara si A es menor o igual que B.
		*/
	 ?>
</body>
</html>