<!DOCTYPE html>
<html>
<head>
	<title>Ejemplo operaciones de comparadores logicos</title>
</head>
<body>
	<h1>Ejemplo operaciones de comparaciones logicas en PHP</h1>
	<?php 
		$a = 8;
		$b = 3;
		$c = 3;
		echo ($a == $b) && ($c > $b), "<br>";
		echo ($a == $b) || ($b == $c), "<br>";
		echo ! ($b <= $c)b, "<br>";

		/*
			&& Compara si ambos son iguales
			|| Compara si cualquiera es verdadero
		*/
	 ?>
</body>
</html>